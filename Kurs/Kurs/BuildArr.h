#pragma once
class BuildArr
{
public:
	BuildArr(int N);
	void InitializeArra();
	int *GetOrders();
	int *GetRevers();
	int *GetARandoms();
	int GetLength();
	int *GetArrs(int i);
	~BuildArr();
};

