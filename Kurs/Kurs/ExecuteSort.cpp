#include "stdafx.h"
#include "ExecuteSort.h"
#include "BuildArr.h"
#include "Sort.h"
#include <ctime>
#include <iostream>
#include <windows.h> 
#include <Windows.h>
#include <ctime>

using namespace std;

int _range;
int _version;

double *times = new double[_range, _range];
int *displacements = new int[_range, _range];
int *comparisons = new int[_range, _range];

double timeSelectSort;
double timeSheikerSort;
double timeQuickSort;
int displacementsSelectSort;
int displacementsSheikerSort;
int displacementsQuickSort;
int comparisonsSelectSort;
int comparisonsSheikerSort;
int comparisonsQuickSort;

//double PCFreq = 0.0;
__int64 CounterStart = 0;

std::clock_t start;
double duration;

BuildArr *bundlArrs;

Sort *mainSort = new Sort;

ExecuteSort::ExecuteSort(int range)
{
	_range = range;
}

void ExecuteSort::CustomExeSort(int L) 
{	
	bundlArrs = new BuildArr(L);

	for (int i = 0; i < 3; i++)
	{
		bundlArrs->InitializeArra();
		mainSort->CustomSort(bundlArrs->GetArrs(i), bundlArrs->GetLength());

		start = std::clock();
		mainSort->ExecuteQuickSort();
		times[0, i] = (std::clock() - start) / (double)CLOCKS_PER_SEC;
		displacements[0, i] = mainSort->GetDisplacements();
		comparisons[0, i] = mainSort->GetComparisons();
		cout << mainSort->GetComparisons() << "\n";
	}

	for (int i = 0; i < 3; i++)
	{
		bundlArrs->InitializeArra();
		mainSort->CustomSort(bundlArrs->GetArrs(i), bundlArrs->GetLength());

		start = std::clock();
		mainSort->SheikerSort();
		times[1, i] = (std::clock() - start) / (double)CLOCKS_PER_SEC;
		displacements[1, i] = mainSort->GetDisplacements();
		comparisons[1, i] = mainSort->GetComparisons(); 
		cout << mainSort->GetComparisons() << "\n";
	}

	for (int i = 0; i < 3; i++)
	{
		bundlArrs->InitializeArra();
		mainSort->CustomSort(bundlArrs->GetArrs(i), bundlArrs->GetLength());

		start = std::clock();
		mainSort->SelectSort();
		times[2, i] = (std::clock() - start) / (double)CLOCKS_PER_SEC;
		displacements[2, i] = mainSort->GetDisplacements();
		comparisons[2, i] = mainSort->GetComparisons();
		cout << mainSort->GetComparisons() << "\n";
	}

/*

	for (int i = 0; i < 10; i++)
	{
		cout << bundlArrs->GetRevers()[i] << "\n";
	}

	start = std::clock();
	mainSort->SelectSort();
	timeSelectSort = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	comparisonsSelectSort = mainSort->GetComparisons();
	displacementsSelectSort = mainSort->GetDisplacements();
		
	for (int i = 0; i < 10; i++)
	{
		cout << bundlArrs->GetRevers()[i] << "\n";
	}
	bundlArrs->InitializeArra();
	mainSort->CustomSort(bundlArrs->GetRevers(), bundlArrs->GetLength());

	for (int i = 0; i < 10; i++)
	{
			cout << bundlArrs->GetRevers()[i] << "\n";
	}

	start = std::clock();
	mainSort->SheikerSort();
	timeSheikerSort = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	comparisonsSheikerSort = mainSort->GetComparisons();
	displacementsSheikerSort = mainSort->GetDisplacements();

	for (int i = 0; i < 10; i++)
	{
		cout << bundlArrs->GetRevers()[i] << "\n";
	}

	bundlArrs->InitializeArra();
	mainSort->CustomSort(bundlArrs->GetRevers(), bundlArrs->GetLength());

	for (int i = 0; i < 10; i++)
	{
		cout << bundlArrs->GetRevers()[i] << "\n";
	}


	for (int i = 0; i < 10; i++)
	{
		cout << bundlArrs->GetRevers()[i] << "\n";
	}
		*/
}

double* ExecuteSort::GetTimes()
{
	return times;
}
int* ExecuteSort::GetDisplacements()
{
	return displacements;
}
int* ExecuteSort::GetComparisons()
{
	return comparisons;
}

double ExecuteSort::GetTimeSelectSort()
{
	return timeSelectSort;
}
double ExecuteSort::GetTimeSheikerSort()
{
	return timeSheikerSort;
}
double ExecuteSort::GetTimeQuickSort()
{
	return timeQuickSort;
}

int ExecuteSort::GetDisplacementsSelectSort()
{
	return displacementsSelectSort;
}
int ExecuteSort::GetDisplacementsSheikerSort()
{
	return displacementsSheikerSort;
}
int ExecuteSort::GetDisplacementsQuickSort()
{
	return displacementsQuickSort;
}

int ExecuteSort::GetComparisonsSelectSort()
{
	return comparisonsSelectSort;
}
int ExecuteSort::GetComparisonsSheikerSort()
{
	return comparisonsSheikerSort;
}
int ExecuteSort::GetComparisonsQuickSort()
{
	return comparisonsQuickSort;
}

ExecuteSort::~ExecuteSort()
{
}

////void ExecuteSort::StartCounter() {
////	LARGE_INTEGER li;
////	if (!QueryPerformanceFrequency(&li))
////		cout << "QueryPerformanceFrequency failed!\n";
////	PCFreq = double(li.QuadPart) / 1000.0;
////	QueryPerformanceCounter(&li);
////	CounterStart = li.QuadPart;
////}
////double ExecuteSort::GetCounter() {
////	LARGE_INTEGER li;
////	QueryPerformanceCounter(&li);
////	return double(li.QuadPart - CounterStart) / PCFreq;
////}


//
//void ExecuteSort::StartCounter() {
//	CounterStart = HightTime();
//}
//double ExecuteSort::GetCounter() {
//	return HightTime() - CounterStart;
//}
//
//inline _int64 ExecuteSort::HightTime() {
//	_asm {
//		rdtsc
//	}
//}
//
//_int64 pft_start;
//_int64 pft_finish;
//
//#define SPEED_START_CONSOLE pft_start = HightTime();
//#define SPEED_FINISH_CONSOLE pft_finish = HightTime(); cout << pft_finish - pft_start << endl;
