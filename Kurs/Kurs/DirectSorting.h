#pragma once
class DirectSorting
{
public:
	DirectSorting();
	~DirectSorting();
	int FindMin(int N, int Psrav, int startindex, int M[]);
	void Swap(int x, int y, int z);
	void Select_sort(int *a, int length);
};

