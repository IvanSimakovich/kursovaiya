#pragma once
class Sort
{
public:
	Sort();
	~Sort();
	void CustomSort(int *mass, int massLength);
	void Swap(int x, int y);
	int GetComparisons();
	int GetDisplacements();
	void SelectSort();
	void SheikerSort();
	void ExecuteQuickSort();
	void QuickSort(int *a, int first, int last);
};

