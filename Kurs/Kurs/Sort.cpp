﻿#include "stdafx.h"
#include "Sort.h"
#include "BuildArr.h"
#include <iostream>
#include <cstddef>
#include <utility>

int *_mass;
int _massLength;
int displacement;
int comparison;

Sort::Sort()
{
}

Sort::~Sort()
{
}

int Sort::GetDisplacements() {
	return displacement;
}

int Sort::GetComparisons() {
	std::cout << "Comparsion - " << comparison << "\n";
	return comparison;
}

void Sort::CustomSort(int *mass, int massLength) {
	_mass = mass;
	_massLength = massLength;
	comparison = 0;
	displacement = 0;
}

//// Sort of direct sort type
void Sort::SelectSort() {
	comparison = 0;
	displacement = 0;
	for (int i = 0; i < _massLength - 1; i++) {
		int min_i = i;
		for (int j = i + 1; j < _massLength; j++) {
			//Comparsion
			comparison++;
			if (_mass[j] < _mass[min_i])
			{
				min_i = j;
			}
		}
		//swap element
		displacement++;
		int temp = _mass[i];
		_mass[i] = _mass[min_i];
		_mass[min_i] = temp;
	}
}
void Sort::Swap(int x, int y) {
	int t;
	t = x;
	x= y;
	y= t;
	displacement++;
}

////Sort of shaker type
void Sort::SheikerSort()
{
	comparison = 0;
	displacement = 0;
	int left = 0, right = _massLength - 1;
	//Execution loop while left and rigth border don't meet or while in mass is replasing
	do {
		for (int i = left; i < right; i++)
		{
			//if next element is biggest than replase element
			comparison++;
			if (_mass[i + 1] < _mass[i])
			{
				displacement++;
				std::swap(_mass[i], _mass[i + 1]);
				//was replasing
			}
		}
		//move right board
		right--;

		for (int i = right; i > left; i--)
		{
			//if element before this element is biggest than replase element
			comparison++;
			if (_mass[i - 1] >  _mass[i])
			{
				displacement++;
				std::swap(_mass[i - 1], _mass[i]);
				//was replasing
			}
		}
		//move left board
		left++;
	} while (left <= right);
}

void Sort::ExecuteQuickSort() {
	comparison = 0;
	displacement = 0;
	QuickSort(_mass, 0, _massLength-1);
}

////Sort of quick type
void Sort::QuickSort(int *a, int first, int last)
{
	//define left and right border and pivot element
	int i = first, j = last, x = a[(first + last) / 2];
	
	//while don't raise pivot element
	do {
		//search element from left board which less than povit
		while (a[i] < x) i++;
		//search element from right board which biggest than povit
		while (a[j] > x) j--;
		//Compare element
		comparison++;
		if (i <= j) {
			if (i < j) {
				//Swap element
				displacement++;
				std::swap(a[i], a[j]);
			}
			i++;
			j--;
		}
	} while (i <= j);

	//recursive descent
	if (i < last)
		QuickSort(a, i, last);
	if (first < j)
		QuickSort(a, first, j);
}