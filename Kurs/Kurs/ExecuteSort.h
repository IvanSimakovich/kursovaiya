#pragma once
class ExecuteSort
{
public:
	ExecuteSort(int range);
	void StartCounter();
	double GetCounter();
	void CustomExeSort(int N);
	double* GetTimes();
	int* GetDisplacements();
	int* GetComparisons();
	double GetTimeSelectSort();
	double GetTimeSheikerSort();
	double GetTimeQuickSort();
	int GetDisplacementsSelectSort();
	int GetDisplacementsSheikerSort();
	int GetDisplacementsQuickSort();
	int GetComparisonsSelectSort();
	int GetComparisonsSheikerSort();
	int GetComparisonsQuickSort();
	inline _int64 HightTime();
	~ExecuteSort();
};