#include "stdafx.h"
#include <cstdlib>
#include "BuildArr.h"

#include "Kurs.h"
#include "stdafx.h"
#include <iostream>
int _N;
int *orders;
int *revers;
int *aRandoms;
int *arrs[3];

BuildArr::BuildArr(int N)
{
	_N = N;
}

int BuildArr::GetLength()
{
	return _N;
}
int* BuildArr::GetOrders()
{
	return orders;
}
int* BuildArr:: GetRevers()
{
	return revers;
}
int* BuildArr::GetARandoms()
{	
	return aRandoms;
}
int*  BuildArr::GetArrs(int i) {
	return arrs[i];
}


void BuildArr::InitializeArra() {
	orders = new int[_N];
	revers = new int[_N];
	aRandoms = new int[_N];

	for (int i = 0; i < _N ; i++)
	{
		orders[i] = i;
		revers[i] = _N - i;
		aRandoms[i] = rand() % _N + 1;
	}

	arrs[0] = aRandoms;
	arrs[1] = revers;
	arrs[2] = orders;
}

BuildArr::~BuildArr()
{
}

