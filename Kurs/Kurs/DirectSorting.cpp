﻿#include "stdafx.h"
#include "DirectSorting.h"
#include <iostream>

DirectSorting::DirectSorting()
{
}

int DirectSorting::FindMin(int N, int Psrav, int startindex, int M[]) {
	int u, min, lowindex;
	lowindex = startindex;
	min = M[startindex];
	for (int u = startindex+1; u < N; u++)
	{
		Psrav++;
		if (M[u]< min)
		{
			min = M[u]; 
			lowindex = u;
		}
	}

	return lowindex;
}


//Revert element itself
void Swap(int x, int y, int z)
{
	int t;
	t = x;
	x = y;
	y = t;
	z++;
}


DirectSorting::~DirectSorting()
{
}

void DirectSorting::Select_sort(int *a, int length) {
	for (int i = 0; i < length - 1; i++) {
		int min_i = i;
		for (int j = i + 1; j < length; j++) {
			if (a[j] < a[min_i]) 
			{
				min_i = j;
			}
		}
		int temp = a[i];
		a[i] = a[min_i];
		a[min_i] = temp;
	}
}

//void select_sort1(int *a, int length) {
//	for (int i = 0; i < length - 1; i++) {
//		/* устанавливаем начальное значение минимального индекса */
//		int min_i = i;
//		/* находим индекс минимального элемента */
//		for (int j = i + 1; j < length; j++) {
//			if (a[j] < a[min_i]) {
//				min_i = j;
//			}
//		}
//		/* меняем значения местами */
//		int temp = array[i];
//		array[i] = array[min_i];
//		array[min_i] = temp;
//	}
//}

////procedure Pryamoi_vibor(M:Ar); {Сортирует массив с помощью прямого выбора}
////var
////j : integer;
////begin
////for j: = 0 to N - 1 do
////begin
////FindMin(j, m, i);
////swap(M[j], M[i], Pper);
////end
////end;


////
////procedure FindMin(startindex:integer; M:ar; var lowindex : integer); {Перебирает массив начиная с заданного индекса и находим минимальный элемент}
////var min, u:integer;
////begin
////lowindex : = startindex;
////min: = M[startindex];
////for u: = startindex + 1 to N - 1 do
////begin
////inc(Psrav);
////if M[u]< min then
////	begin
////	min : = M[u];
////lowindex: = u;
////end;
////end;
////end;
////…
////procedure swap(var x, y, z: integer); {Меняем местами заданные элементы массива}
////var t : integer;
////begin
////t : = x;
////x: = y;
////y: = t;
////inc(z);
////end;
////…
////procedure Pryamoi_vibor(M:Ar); {Сортирует массив с помощью прямого выбора}
////var
////j : integer;
////begin
////for j: = 0 to N - 1 do
////begin
////FindMin(j, m, i);
////swap(M[j], M[i], Pper);
////end
////end;

