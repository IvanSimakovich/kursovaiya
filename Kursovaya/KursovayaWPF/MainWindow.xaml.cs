﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using Kursovaya;

namespace KursovayaWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<int> number = new List<int> { 100, 5000, 30000 };
        private List<Diagram> _diagramm = new List<Diagram>();
        private Table _table;
        public string Header1 { get; set; }
        public MainWindow()
        {
            List<ExecuteSort> sorts = new List<ExecuteSort>();

            for (int i = 0, x = 0; i < number.Count; i++, x++)
            {
                sorts.Add(new ExecuteSort(number[i]));
                _diagramm.Add(new Diagram(sorts[x].Mass));
            }

            InitializeComponent();
            Diagram1.Content = _diagramm[0];
            Diagrams1.Header = number[0].ToString();
            Diagram2.Content = _diagramm[1];
            Diagrams2.Header = number[1].ToString();
            Diagram3.Content = _diagramm[2];
            Diagrams3.Header = number[2].ToString();
            BubbleTable.Header = "BubbleSortTable";
            BubbleTableContent.Content = new Table(sorts, 0, number);
            QuickTable.Header = "QuickSortTable";
            QuickTableContent.Content = new Table(sorts, 1, number);
            RadixTable.Header = "RadixSortTable";
            RadixTableContent.Content = new Table(sorts, 2, number);
        }
    }
}
