﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Kursovaya;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace KursovayaWPF
{
    /// <summary>
    /// Interaction logic for Diagram.xaml
    /// </summary>
    public partial class Diagram : UserControl
    {
        private PlotModel _model;
        private Diagram _diagramm;
        public PlotModel MyModel1 { get; private set; }
        public PlotModel MyModel2 { get; private set; }
        public PlotModel MyModel3 { get; private set; }

        public Diagram(List<List<Massiv>> sort)
        {
            InitializeComponent();
            this.DataContext = this;

            _model = new PlotModel
            {
                Title = "BubbleSort",
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0
            };

            var s1 = new ColumnSeries { Title = "Ordered array", StrokeColor = OxyColors.Black, StrokeThickness = 1, FillColor = OxyColor.FromArgb(255, 255, 0, 6) };
            s1.Items.Add(new ColumnItem { Value = sort[0][0].Comparsion });
            s1.Items.Add(new ColumnItem { Value = sort[0][0].Displacement });
            s1.Items.Add(new ColumnItem { Value = sort[0][0].Time.Ticks  });
                                                        
            var s2 = new ColumnSeries { Title = $"Revert ordered array", StrokeColor = OxyColors.Black, StrokeThickness = 1 };
            s2.Items.Add(new ColumnItem { Value = sort[0][1].Comparsion });
            s2.Items.Add(new ColumnItem { Value = sort[0][1].Displacement });
            s2.Items.Add(new ColumnItem { Value = sort[0][1].Time.Ticks  });
                                                        
            var s3 = new ColumnSeries { Title = $"Random ordered sort", StrokeColor = OxyColors.Black, StrokeThickness = 1 };
            s3.Items.Add(new ColumnItem { Value = sort[0][2].Comparsion });
            s3.Items.Add(new ColumnItem { Value = sort[0][2].Displacement });
            s3.Items.Add(new ColumnItem { Value = sort[0][2].Time.Ticks });

            var categoryAxis = new CategoryAxis { Position = AxisPosition.Bottom };
            categoryAxis.Labels.Add("Comparsion");
            categoryAxis.Labels.Add("Displacement");
            categoryAxis.Labels.Add("Time");
            var valueAxis = new LinearAxis { Position = AxisPosition.Left, MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0 };
            _model.Series.Add(s1);
            _model.Series.Add(s2);
            _model.Series.Add(s3);
            _model.Axes.Add(categoryAxis);
            _model.Axes.Add(valueAxis);

            this.MyModel1 = _model;
            _model = new PlotModel
            {
                Title = "QuickSort",
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0
            };

            s1 = new ColumnSeries { Title = "Ordered array", StrokeColor = OxyColors.Black, StrokeThickness = 1, FillColor = OxyColor.FromArgb(255, 255, 0, 6) };
            s1.Items.Add(new ColumnItem { Value = sort[1][0].Comparsion });
            s1.Items.Add(new ColumnItem { Value = sort[1][0].Displacement });
            s1.Items.Add(new ColumnItem { Value = sort[1][0].Time.Ticks });

            s2 = new ColumnSeries { Title = $"Revert ordered array", StrokeColor = OxyColors.Black, StrokeThickness = 1 };
            s2.Items.Add(new ColumnItem { Value = sort[1][1].Comparsion });
            s2.Items.Add(new ColumnItem { Value = sort[1][1].Displacement });
            s2.Items.Add(new ColumnItem { Value = sort[1][1].Time.Ticks });

            s3 = new ColumnSeries { Title = $"Random ordered sort", StrokeColor = OxyColors.Black, StrokeThickness = 1 };
            s3.Items.Add(new ColumnItem { Value = sort[1][2].Comparsion });
            s3.Items.Add(new ColumnItem { Value = sort[1][2].Displacement });
            s3.Items.Add(new ColumnItem { Value = sort[1][2].Time.Ticks });

            categoryAxis = new CategoryAxis { Position = AxisPosition.Bottom };
            categoryAxis.Labels.Add("Comparsion");
            categoryAxis.Labels.Add("Displacement");
            categoryAxis.Labels.Add("Time");
            valueAxis = new LinearAxis { Position = AxisPosition.Left, MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0 };
            _model.Series.Add(s1);
            _model.Series.Add(s2);
            _model.Series.Add(s3);
            _model.Axes.Add(categoryAxis);
            _model.Axes.Add(valueAxis);

            this.MyModel2 = _model; _model = new PlotModel
            {
                Title = "RadixSort",
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0
            };
            
            s1 = new ColumnSeries { Title = "Ordered array", StrokeColor = OxyColors.Black, StrokeThickness = 1, FillColor = OxyColor.FromArgb(255, 255, 0, 6) };
            s1.Items.Add(new ColumnItem { Value = sort[2][0].Comparsion });
            s1.Items.Add(new ColumnItem { Value = sort[2][0].Displacement });
            s1.Items.Add(new ColumnItem { Value = sort[2][0].Time.Ticks });

            s2 = new ColumnSeries { Title = $"Revert ordered array", StrokeColor = OxyColors.Black, StrokeThickness = 1 };
            s2.Items.Add(new ColumnItem { Value = sort[2][1].Comparsion });
            s2.Items.Add(new ColumnItem { Value = sort[2][1].Displacement });
            s2.Items.Add(new ColumnItem { Value = sort[2][1].Time.Ticks });

            s3 = new ColumnSeries { Title = $"Random ordered sort", StrokeColor = OxyColors.Black, StrokeThickness = 1 };
            s3.Items.Add(new ColumnItem { Value = sort[2][2].Comparsion });
            s3.Items.Add(new ColumnItem { Value = sort[2][2].Displacement });
            s3.Items.Add(new ColumnItem { Value = sort[2][2].Time.Ticks });

            categoryAxis = new CategoryAxis { Position = AxisPosition.Bottom };
            categoryAxis.Labels.Add("Comparsion");
            categoryAxis.Labels.Add("Displacement");
            categoryAxis.Labels.Add("Time");
            valueAxis = new LinearAxis { Position = AxisPosition.Left, MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0 };
            _model.Series.Add(s1);
            _model.Series.Add(s2);
            _model.Series.Add(s3);
            _model.Axes.Add(categoryAxis);
            _model.Axes.Add(valueAxis);

            this.MyModel3 = _model;
        }
    }
}