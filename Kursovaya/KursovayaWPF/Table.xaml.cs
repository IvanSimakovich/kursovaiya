﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kursovaya;

namespace KursovayaWPF
{
    public partial class Table : UserControl
    {
        public Table(List<ExecuteSort> sorts, int keyOfSort, List<int> number)
        {
            InitializeComponent();
            this.DataContext = this;
            Rang1.Text = number[0].ToString();
            Rang2.Text = number[1].ToString();
            Rang3.Text = number[2].ToString();
            OrdD500.Text = sorts[0].Mass[keyOfSort][0].Displacement.ToString();
            OrdD5000.Text = sorts[1].Mass[keyOfSort][0].Displacement.ToString();
            OrdD50000.Text = sorts[2].Mass[keyOfSort][0].Displacement.ToString();
            OrdC500.Text = sorts[0].Mass[keyOfSort][0].Comparsion.ToString();
            OrdC5000.Text = sorts[1].Mass[keyOfSort][0].Comparsion.ToString();
            OrdC50000.Text = sorts[2].Mass[keyOfSort][0].Comparsion.ToString();
            OrdT500.Text = sorts[0].Mass[keyOfSort][0].Time.Ticks.ToString();
            OrdT5000.Text = sorts[1].Mass[keyOfSort][0].Time.Ticks.ToString();
            OrdT50000.Text = sorts[2].Mass[keyOfSort][0].Time.Ticks.ToString();
            InvD500.Text = sorts[0].Mass[keyOfSort][1].Displacement.ToString();
            InvD5000.Text = sorts[1].Mass[keyOfSort][1].Displacement.ToString();
            InvD50000.Text = sorts[2].Mass[keyOfSort][1].Displacement.ToString();
            InvC500.Text = sorts[0].Mass[keyOfSort][1].Comparsion.ToString();
            InvC5000.Text = sorts[1].Mass[keyOfSort][1].Comparsion.ToString();
            InvC50000.Text = sorts[2].Mass[keyOfSort][1].Comparsion.ToString();
            InvT500.Text = sorts[0].Mass[keyOfSort][1].Time.Ticks.ToString();
            InvT5000.Text = sorts[1].Mass[keyOfSort][1].Time.Ticks.ToString();
            InvT50000.Text = sorts[2].Mass[keyOfSort][1].Time.Ticks.ToString();
            RanD500.Text = sorts[0].Mass[keyOfSort][2].Displacement.ToString();
            RanD5000.Text = sorts[1].Mass[keyOfSort][2].Displacement.ToString();
            RanD50000.Text = sorts[2].Mass[keyOfSort][2].Displacement.ToString();
            RanC500.Text = sorts[0].Mass[keyOfSort][2].Comparsion.ToString();
            RanC5000.Text = sorts[1].Mass[keyOfSort][2].Comparsion.ToString();
            RanC50000.Text = sorts[2].Mass[keyOfSort][2].Comparsion.ToString();
            RanT500.Text = sorts[0].Mass[keyOfSort][2].Time.Ticks.ToString();
            RanT5000.Text = sorts[1].Mass[keyOfSort][2].Time.Ticks.ToString();
            RanT50000.Text = sorts[1].Mass[keyOfSort][2].Time.Ticks.ToString();

        }
    }
}
