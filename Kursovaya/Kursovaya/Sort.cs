﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Kursovaya
{
    public class Sort
    {
        public Sort()
        {
        }

        public delegate void InternalDelFormethod(List<int> a);
        private int _comparsion;
        private int _displacement;

        public int Displacement
        {
            get { return _displacement; }
            set { _displacement = value; }
        }

        public int Comparison
        {
            get { return _comparsion; }
            set { _comparsion = value; }
        }
        
        // change them in places
        private List<int> Swap(List<int> arrs, int x, int y)
        {
            int t;
            t = arrs[x];
            arrs[x] = arrs[y];
            arrs[y] = t;
            _displacement++;
            return arrs;
        }

        public void BubbleSort(List<int> arrs)
        {
            _displacement = 0;
            _comparsion = 0;
            for (int i = 0; i < arrs.Count; i++)
            {
                for (int j = 0; j < arrs.Count - i - 1; j++)
                {
                    _comparsion++;
                    if (arrs[j] > arrs[j + 1])
                    {
                        int temp = arrs[j];
                        arrs[j] = arrs[j + 1];
                        arrs[j + 1] = temp;
                        _displacement++;
                    }
                }
            }
        }

        public void RadixSort(List<int> arr)
        {
            _comparsion = 0;
            _displacement = 0;
            var range = 10;
            int length = 0;
            var tempLength = arr.Count;
            while (tempLength > 0)
            {
                tempLength = tempLength / 10; ++length;
            }

            ArrayList[] lists = new ArrayList[range];
            for (int i = 0; i < range; ++i)
                lists[i] = new ArrayList();

            for (int step = 0; step < length; ++step)
            {
                //распределение по спискам
                for (int i = 0; i < arr.Count; ++i)
                {
                    _comparsion++;
                    int temp = (arr[i] % (int)Math.Pow(range, step + 1)) /
                               (int)Math.Pow(range, step);
                    _displacement++;
                    lists[temp].Add(arr[i]);
                }
                //сборка
                int k = 0;
                for (int i = 0; i < range; ++i)
                {
                    for (int j = 0; j < lists[i].Count; ++j)
                    {
                        _displacement++;
                        arr[k++] = (int)lists[i][j];
                    }
                }
                for (int i = 0; i < range; ++i)
                    lists[i].Clear();
            }
        }

        public void QuickSort(List<int> arrs)
        {
            _comparsion = 0;
            _displacement = 0;
            QuickSortInnerPart(arrs, 0, arrs.Count - 1);
        }

        public void QuickSortInnerPart(List<int> a, int first, int last)
        {
            //define left and right border and pivot element
            int i = first, j = last, x = a[(first + last) / 2];

            //while don't raise pivot element
            do
            {
                //search element from left board which less than povit
                while (a[i] < x) i++;
                //search element from right board which biggest than povit
                while (a[j] > x) j--;
                //Compare element
                _comparsion++;
                if (i <= j)
                {
                    if (i < j)
                    {
                        //Swap element
                        Swap(a, i, j);
                    }
                    i++;
                    j--;
                }
            } while (i <= j);

            //recursive descent
            if (i < last)
                QuickSortInnerPart(a, i, last);
            if (first < j)
                QuickSortInnerPart(a, first, j);
        }
    }
}
