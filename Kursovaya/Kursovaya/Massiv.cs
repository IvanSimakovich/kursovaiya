﻿using System;

namespace Kursovaya
{
    public class Massiv
    {
        public string Name { get; set; }

        public int Comparsion { get; set; }

        public int Displacement { get; set; }

        public DateTime Time { get; set; }
    }
}
