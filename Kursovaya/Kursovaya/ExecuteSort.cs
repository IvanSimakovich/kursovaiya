﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Kursovaya
{
    public class ExecuteSort
    {
        private Sort mainSort = new Sort();
        private BuildArr bundlArrs;
        private Stopwatch stopWatch = new Stopwatch();

        private delegate void InDelFormethod(List<int> a);

        public List<List<Massiv>> Mass = new List<List<Massiv>>();
        private int _rang;

        public ExecuteSort(int rang)
        {
            _rang = rang;
            CustomExeSort(rang);
        }

        public int Rang
        {
            get { return _rang; }
            set { _rang = value; }
        }

        public void CustomExeSort(int rang)
        {
            bundlArrs = new BuildArr(rang);
            bundlArrs.InitializeArra();
            int[] x;
            int q = 0;


            BuildArr bundlArrs1 = new BuildArr(bundlArrs);
            BuildArr bundlArrs2 = new BuildArr(bundlArrs);
            BuildArr bundlArrs3 = new BuildArr(bundlArrs);
            
            InDelFormethod xxx;

            xxx = mainSort.BubbleSort;
            ForeachArr(0, xxx, bundlArrs1.Arrs);

            xxx = mainSort.QuickSort;
            ForeachArr(1, xxx, bundlArrs2.Arrs);

            xxx = mainSort.RadixSort;
            ForeachArr(2, xxx, bundlArrs3.Arrs);
        }

        private void ForeachArr(int numberMas, InDelFormethod _method, List<List<int>> bundlArrs)
        {
            var current = new List<Massiv>();
            var q = 0;
            while ( q < bundlArrs.Count)
            {
                if (q != 0)
                {
                    stopWatch.Start();
                    _method(bundlArrs[q]);
                    stopWatch.Stop();
                    var ts = stopWatch.Elapsed;
                    current.Add(new Massiv
                    {
                        Comparsion = mainSort.Comparison,
                        Displacement = mainSort.Displacement,
                        Name = (q == 1) ? "Normal" : (q == 2) ? "Revert" : "Random",
                        Time = new DateTime().AddTicks(+ts.Ticks),
                    });
                }
                else
                {
                    stopWatch.Start();
                    _method(bundlArrs[q]);
                    stopWatch.Stop();
                    var ts = stopWatch.Elapsed;
                }
                q++;
                stopWatch.Reset();
            }
            Mass.Add(current);
        }
    }
}
