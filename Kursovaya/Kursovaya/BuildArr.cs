﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursovaya
{
    class BuildArr
    {
        private List<int> orders;
        private List<int> reverts;
        private List<int> randoms;
        private List<List<int>> arrs;
        private int _n;

        public BuildArr(int n)
        {
            _n = n;
            orders = new List<int>();
            reverts = new List<int>();
            randoms = new List<int>();
            arrs = new List<List<int>>();
        }
        public BuildArr(BuildArr other)
        {
            this._n = other._n;
            this.orders = new List<int>(other.orders);
            this.reverts = new List<int>(other.reverts);
            this.arrs = new List<List<int>>();
            other.Arrs.ForEach(o => {this.arrs.Add(new List<int>(o));});
        }

        public List<int> Orders
        {
            get { return orders; }
            set { orders = value; }
        }
        public List<int> Reverts
        {
            get { return reverts; }
            set { reverts = value; }
        }
        public List<int> Randoms
        {
            get { return randoms; }
            set { randoms = value; }
        }
        public List<List<int>> Arrs
        {
            get { return arrs; }
            set { arrs = value; }
        }

        public void InitializeArra()
        {
            Random rand = new Random();
            for (int i = 0; i < _n; i++)
            {
                orders.Add(i+1);
                reverts.Add(_n - i);
                randoms.Add(rand.Next(_n+1));
            }

            arrs.Add(randoms);
            
            arrs.Add(orders);
            arrs.Add(reverts);
            arrs.Add(randoms);
        }
    }
}
